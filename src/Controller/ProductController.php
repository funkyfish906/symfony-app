<?php

namespace App\Controller;

use App\Entity\Product;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class ProductController extends AbstractController
{
    /**
     * @Route("/products", name="products")
     */

    public function index()
    {
        $repository = $this->getDoctrine()->getRepository(Product::class);

        $products = $repository->findAll();

        //dump($products); exit();

        return $this->render('product/index.html.twig', [
            'products' => $products,
        ]);
    }

    /**
     * @Route("/products/new", name="products_new")
     */

    public function new()
    {

        $product = new Product();


        $form = $this->createFormBuilder($product)
            ->setAction($this->generateUrl('products_create'))
            ->setMethod('POST')
            ->add('name', TextType::class)
            ->add('price', TextType::class)
            ->add('description', TextareaType::class)
            ->add('save', SubmitType::class, array('label' => 'Create Task'))
            ->getForm();

        if ($form->isSubmitted() && $form->isValid()) {

            $errors = $form->getErrors();

            if (!empty($errors)){
                return $this->render('product/create.html.twig', [
                    'form' => $form->createView(), 'errors' => $errors
                ]);
            }

            $product = $form->getData();

            $em = $this->getDoctrine()->getManager();
            $em->persist($product);
            $em->flush();

            return $this->redirectToRoute('product');
        }


        return $this->render('product/create.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/products/create", name="products_create")
     */
    public function create()
    {

        return $this->render('product/create.html.twig');
    }

        /**
     * @Route("/products/save", name="products_save")
     */

    public function save(Request $request)
    {

        $name = $request->request->get('name');

        $price = $request->request->get('price');

        $description = $request->request->get('description');

        $em = $this->getDoctrine()->getManager();

        $product = new Product();
        $product->setName($name);
        $product->setPrice($price);
        $product->setDescription($description);

        // скажите Doctrine, что вы (в итоге) хотите сохранить Товар (пока без запросов)
        //
        $em->persist($product);

        // на самом деле выполнить запросы (т.е. запрос INSERT)
        $em->flush();

        return $this->redirectToRoute('products');
    }
}
