<?php
namespace App\Controller\Admin;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProductController extends AbstractController
{
    /**
     * @Route("/admin")
     */
    public function index()
    {
        return new Response('<html><body>Admin page!</body></html>');
    }
}